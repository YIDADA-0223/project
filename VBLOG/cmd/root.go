package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	_ "gitlab.com/YIDADA-0223/VBLOG/apps"
	initCmd "gitlab.com/YIDADA-0223/VBLOG/cmd/init"
	"gitlab.com/YIDADA-0223/VBLOG/cmd/start"
	"gitlab.com/YIDADA-0223/VBLOG/conf"
	"gitlab.com/YIDADA-0223/VBLOG/ioc"
)

// 定义全局configpath变量
var (
	configPath string
)

// 定义根命令
var RootCmd = &cobra.Command{
	Use:   "vblog",         // 命令名称
	Short: "vblog service", // 命令简介
	Run: func(cmd *cobra.Command, args []string) { // 运行命令
		// Do Stuff Here
		if len(args) > 0 { // 如果有参数
			if args[0] == "version" {
				fmt.Println("v0.0.1")
			}
		} else {
			cmd.Help() // 显示帮助
		}

	},
}

func Execute() error {
	// 初始化需要执行的逻辑
	cobra.OnInitialize(func() {
		// 1. 加载配置
		cobra.CheckErr(conf.LoadConfigFromYaml(configPath))
		// 2. 初始化Ioc
		cobra.CheckErr(ioc.Controller.Init())
		// 3. 初始化Api
		cobra.CheckErr(ioc.Api.Init())
	})
	return RootCmd.Execute()
}
func init() {
	// 定义全局参数
	RootCmd.PersistentFlags().StringVarP(&configPath, "config", "c", "etc/application.yml", "config file path")
	// 注册子命令
	RootCmd.AddCommand(initCmd.Cmd)
	RootCmd.AddCommand(start.Cmd)
}

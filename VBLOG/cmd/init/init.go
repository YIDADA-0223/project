package init

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	i string
)

// 定义子命令
var Cmd = &cobra.Command{
	Use:   "init",
	Short: "init vblog service",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("init vblog service")
	},
}

func init() {
	Cmd.Flags().StringVarP(&i, "init", "i", "init", "init flag")
}

package start

import (
	"os"

	"github.com/spf13/cobra"
	_ "gitlab.com/YIDADA-0223/VBLOG/apps"
	"gitlab.com/YIDADA-0223/VBLOG/conf"
)

// 定义全局字符串变量，用于存储命令行参数的值
var (
	testParam string
)
var Cmd = &cobra.Command{
	Use:   "start",
	Short: "start vblog",
	Run: func(cmd *cobra.Command, args []string) {
		// 1.加载配置
		configPath := os.Getenv("CONFIG_PATH")
		if configPath == "" {
			configPath = "etc/application.yml"
		}
		// 4.服务启动
		cobra.CheckErr(conf.C().Application.Start())
	},
}

func init() {
	// 注册命令行参数
	Cmd.Flags().StringVarP(&testParam, "test", "t", "test", "test flag")
}

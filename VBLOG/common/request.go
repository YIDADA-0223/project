package common

type UPDATE_MODE int

const (
	UPDATE_MODE_PUT = iota
	UPDATE_MODE_PATCH
)

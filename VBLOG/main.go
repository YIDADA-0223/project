package main

import (
	"gitlab.com/YIDADA-0223/VBLOG/cmd"

	// 业务对象注册
	_ "gitlab.com/YIDADA-0223/VBLOG/apps"
)

func main() {
	if err := cmd.Execute(); err != nil {
		panic(err)
	}
}

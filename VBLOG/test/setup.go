package test

import (
	"github.com/spf13/cobra"
	_ "gitlab.com/YIDADA-0223/VBLOG/apps"
	"gitlab.com/YIDADA-0223/VBLOG/conf"
	"gitlab.com/YIDADA-0223/VBLOG/ioc"
)

func DevelopmentSetup() {
	// 1.加载配置
	if err := conf.LoadConfigFromEnv(); err != nil {
		panic(err)
	}
	// 2.初始化Ioc
	cobra.CheckErr(ioc.Controller.Init())
	// 3.初始化API
	cobra.CheckErr(ioc.Api.Init())
}

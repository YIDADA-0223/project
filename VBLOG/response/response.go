package response

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/YIDADA-0223/VBLOG/exception"
)

// 怎么是执行成功，把对象转为 HTTP Reponse
func Success(data any, c *gin.Context) {
	c.JSON(http.StatusOK, data)
}

// 统一返回的数据结构：APiException

func Failed(err error, c *gin.Context) {
	// 非200状态，接口报错，返回内容：ApiException对象
	httpCode := http.StatusInternalServerError
	if v, ok := err.(*exception.ApiException); ok {
		if v.HttpCode != 0 {
			httpCode = v.HttpCode
		}
	} else {
		// 非业务异常，支持转化为指定的内部报错异常
		err = exception.ErrServerInternal(err.Error())
	}
	c.JSON(httpCode, err)
	c.Abort()
}

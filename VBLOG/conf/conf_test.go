package conf_test

import (
	"os"
	"testing"

	"gitlab.com/YIDADA-0223/VBLOG/conf"
)

func TestYaml(t *testing.T) {
	t.Log(conf.Default().ToYaml())
}

func TestConfigYaml(t *testing.T) {
	err := conf.LoadConfigFromYaml("./application.yml")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.C().ToYaml())
}
func TestConfigEnv(t *testing.T) {
	os.Setenv("DATASOURCE_USERNAME", "QUYI")
	err := conf.LoadConfigFromEnv()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.C().ToYaml())
}

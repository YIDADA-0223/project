package ioc

// 注入容器接口
type Container interface {
	// 注册容器对象
	Registry(name string, obj Object)
	// 获取容器对象
	Get(name string) any
	// 容器初始化
	Init() error
}

// 容器初始化接口
type Object interface {
	Init() error
}

package ioc_test

import (
	"testing"
	// 引入依赖对象
	user "gitlab.com/YIDADA-0223/VBLOG/apps/user/impl"
	"gitlab.com/YIDADA-0223/VBLOG/ioc"
)

func TestRegistry(t *testing.T) {
	// 在ioc包中调用容器对象并注册对象，提供名称和对象参数
	ioc.Controller.Registry("user", &user.UserServiceImpl{})
	// 调用Get方法获取对象，并打印地址
	t.Logf("%p", ioc.Controller.Get("user"))
}

package ioc

// 定义全局的容器demo
var Controller Container = &MapContainer{
	name:   "Controller",
	storge: make(map[string]Object),
}

// 定义全局的Api容器demo
var Api Container = &MapContainer{
	name:   "Api",
	storge: make(map[string]Object),
}

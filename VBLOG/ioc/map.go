package ioc

import "fmt"

// 设置容器基本信息
type MapContainer struct {
	name   string
	storge map[string]Object
}

// 注册对象，将对象注册到容器中
func (c *MapContainer) Registry(name string, obj Object) {
	c.storge[name] = obj
}

// 获取对象，根据名称获取对象
func (c *MapContainer) Get(name string) any {
	return c.storge[name]
}

// 调用所有被托管对象的init方法，对象进行初始化，并打印初始化日志
func (c *MapContainer) Init() error {
	for k, v := range c.storge {
		if err := v.Init(); err != nil {
			return fmt.Errorf("%s init error %s", k, err)
		}
		fmt.Printf("[%s] %s init success\n", c.name, k)
	}
	return nil
}

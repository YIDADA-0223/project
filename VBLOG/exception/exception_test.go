package exception_test

import (
	"testing"

	"gitlab.com/YIDADA-0223/VBLOG/exception"
)

// 他返回的是构造函数的返回值
func CheckIsError() error {
	return exception.NewApiException(50001, "用户名或者密码不正确")
}

func TestIsError(t *testing.T) {
	err := CheckIsError()
	// t.Log(err)
	//直接打印会直接打印报错信息，并不会打印报错码，需要使用断言
	if v, ok := err.(*exception.ApiException); ok {
		t.Log(v.Code)
		t.Log(v.String())
	}
}

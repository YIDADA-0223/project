package exception

import "encoding/json"

//构造函数用于初始化
func NewApiException(code int, message string) *ApiException {
	return &ApiException{
		Code:    code,
		Message: message,
	}
}

type ApiException struct {
	// 业务异常码
	Code int `json:"code"`
	// 异常描述信息
	Message string `json:"message"`
	// 不会出现在Boyd里面，序列化成JSON，http response进行set
	HttpCode int `json:"-"`
}

//重写Error方法
func (e *ApiException) Error() string {
	return e.Message
}

//序列化控制打印效果
func (e *ApiException) String() string {
	dj, _ := json.MarshalIndent(e, "", " ")
	return string(dj)
}
func (e *ApiException) WithMessage(msg string) *ApiException {
	e.Message = msg
	return e
}
func (e *ApiException) WithHttpCode(httpCode int) *ApiException {
	e.HttpCode = httpCode
	return e
}

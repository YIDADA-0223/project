import axios from 'axios'

export default axios.create({
  // 因为开启了代理，直接使用前端页面URL地址，由vite进行代理到后端
  baseURL: '',
  timeout: 5000
})

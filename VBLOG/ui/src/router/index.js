import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'LoginPage',
      component: () => import('@/views/login/LoginPage.vue')
    },
    {
      path: '/backend',
      name: 'BackendLayout',
      component: () => import('../views/backend/BackendLayout.vue'),
      children: [
        {
          path: 'blogs/list',
          name: 'BackendBlogEdit',
          component: () => import('../views/backend/blogs/ListPage.vue')
        },
        {
          path: 'blogs/edit',
          name: 'BackendBlogEdit',
          component: () => import('../views/backend/blogs/EditPage.vue')
        }
      ]
    },
    {
      path: '/frontend',
      name: 'FrontendLayout',
      component: () => import('../views/frontend/FrontendLayout.vue'),
      redirect: { name: 'FrontendBlogList' },
      children: [
        {
          path: 'blogs/list',
          name: 'FrontendBlogList',
          component: () => import('../views/frontend/blogs/ListPage.vue')
        },
        {
          path: 'blogs/detail',
          name: 'FrontendBlogDetail',
          component: () => import('../views/frontend/blogs/DetailPage.vue')
        }
      ]
    }
  ]
})

export default router

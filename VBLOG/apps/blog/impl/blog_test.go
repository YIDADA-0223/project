package impl_test

import (
	"context"
	"testing"

	"gitlab.com/YIDADA-0223/VBLOG/apps/blog"
	"gitlab.com/YIDADA-0223/VBLOG/common"
	"gitlab.com/YIDADA-0223/VBLOG/ioc"
	"gitlab.com/YIDADA-0223/VBLOG/test"
)

var (
	serviceImpl blog.Service
	ctx         = context.Background()
)

// 招对象
func init() {
	test.DevelopmentSetup()
	serviceImpl = ioc.Controller.Get(blog.AppName).(blog.Service)
}

func TestCreateBlog(t *testing.T) {
	req := blog.NewCreateBlogRequest()
	req.Title = "test"
	req.Content = "测试博客内容"
	req.Author = "test"
	ins, err := serviceImpl.CreateBlog(ctx, req)
	if err != nil {
		panic(err)
	}
	t.Log(ins)
}
func TestQueryBlog(t *testing.T) {
	req := blog.NewQueryBlogRequest()
	ins, err := serviceImpl.QueryBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestDescribeBlog(t *testing.T) {
	req := blog.NewDescribeBlogRequest("1")
	ins, err := serviceImpl.DescribeBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
func TestPathUpdateBlog(t *testing.T) {
	req := blog.NewUpdataBlogRequest("1")
	req.UpdateMode = common.UPDATE_MODE_PATCH
	req.Title = "更新1"
	ins, err := serviceImpl.UpdateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
func TestPutUpdateBlog(t *testing.T) {
	req := blog.NewUpdataBlogRequest("1")
	req.UpdateMode = common.UPDATE_MODE_PATCH
	req.Title = "更新后文章标题1"
	req.Author = "patch"
	req.Content = "patch"
	ins, err := serviceImpl.UpdateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
func TestDeleteBlog(t *testing.T) {
	req := blog.NewDeleteBlogRequest("1")
	ins, err := serviceImpl.DeleteBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
func TestUpdateBlogStatus(t *testing.T) {
	req := blog.NewUpdateBlogStatusRequest("1")
	req.SetSatus(blog.STATUS_PUBLISH)
	ins, err := serviceImpl.UpdateBlogStatus(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

package impl

import (
	"context"

	"dario.cat/mergo"
	"gitlab.com/YIDADA-0223/VBLOG/apps/blog"
	"gitlab.com/YIDADA-0223/VBLOG/common"
	"gitlab.com/YIDADA-0223/VBLOG/exception"
)

func (i *BlogServiceImpl) QueryBlog(ctx context.Context, in *blog.QueryBlogRequest) (*blog.BlogSet, error) {
	req := blog.NewBlogSet()
	query := i.db.WithContext(ctx).Table("blogs")
	if in.KeyWord != "" {
		query = query.Where("title like ?", "%"+in.KeyWord+"%")
	}
	err := query.Limit(in.PageSize).Offset(in.Offset()).Find(&req.Items).Error
	if err != nil {
		return nil, err
	}
	err = query.Count(&req.Total).Error
	if err != nil {
		return nil, err
	}
	return req, nil
}
func (i *BlogServiceImpl) DescribeBlog(ctx context.Context, in *blog.DescribeBlogRequest) (*blog.Blog, error) {
	ins := blog.NewBlog()
	err := i.db.WithContext(ctx).Where("id = ?", in.BlogId).First(ins).Error
	if err != nil {
		return nil, err
	}
	return ins, nil
}

func (i *BlogServiceImpl) CreateBlog(ctx context.Context, in *blog.CreateBlogRequest) (*blog.Blog, error) {
	ins := blog.NewBlog()
	ins.CreateBlogRequest = in
	err := i.db.WithContext(ctx).Create(ins).Error
	if err != nil {
		return nil, err
	}
	return ins, nil
}

func (i *BlogServiceImpl) UpdateBlog(ctx context.Context, in *blog.UpdateBlogRequest) (*blog.Blog, error) {
	// 1. 先把需要更新的对象查询处理
	ins, err := i.DescribeBlog(ctx, blog.NewDescribeBlogRequest(in.BlogId))
	if err != nil {
		return nil, err
	}
	// 2.req，update
	switch in.UpdateMode {
	// 全量更新,你传什么就保存什么
	case common.UPDATE_MODE_PUT:
		ins.CreateBlogRequest = in.CreateBlogRequest
	// 增量更新，只更新有变化的字段
	case common.UPDATE_MODE_PATCH:
		if in.Author != "" {
			ins.Author = in.Author
		}
		if in.Content != "" {
			ins.Content = in.Content
		}
		err := mergo.MapWithOverwrite(ins.CreateBlogRequest, in.CreateBlogRequest)
		if err != nil {
			return nil, err
		}
	}
	// 校验更新的字段
	if err := ins.CreateBlogRequest.Validate(); err != nil {
		return nil, exception.ErrValidateFailed(err.Error())
	}
	// 执行更新,增量
	err = i.db.WithContext(ctx).Table("blogs").Save(ins).Error
	if err != nil {
		return nil, err
	}
	return ins, nil
}

// 文章删除
func (i *BlogServiceImpl) DeleteBlog(ctx context.Context, in *blog.DeleteBlogRequest) (*blog.Blog, error) {
	ins, err := i.DescribeBlog(ctx, blog.NewDescribeBlogRequest(in.BlogId))
	if err != nil {
		return nil, err
	}
	err = i.db.WithContext(ctx).Table("blogs").Where("id = ?", in.BlogId).Delete(&blog.Blog{}).Error
	if err != nil {
		return nil, err
	}
	return ins, nil
}

// 文章发布
func (i *BlogServiceImpl) UpdateBlogStatus(ctx context.Context, in *blog.UpdateBlogStatusRequest) (*blog.Blog, error) {
	ins, err := i.DescribeBlog(ctx, blog.NewDescribeBlogRequest(in.BlogId))
	if err != nil {
		return nil, err
	}
	ins.ChangedBlogStatusRequest = in.ChangedBlogStatusRequest
	ins.SetSatus(in.Status)
	err = i.db.WithContext(ctx).Table("blogs").Where("id = ?", in.BlogId).Updates(ins.ChangedBlogStatusRequest).Error
	if err != nil {
		return nil, err
	}
	return ins, nil
}

package impl

import (
	"gitlab.com/YIDADA-0223/VBLOG/apps/blog"
	"gitlab.com/YIDADA-0223/VBLOG/conf"
	"gitlab.com/YIDADA-0223/VBLOG/ioc"
	"gorm.io/gorm"
)

func init() {
	ioc.Controller.Registry(blog.AppName, &BlogServiceImpl{})
}

type BlogServiceImpl struct {
	db *gorm.DB
}

func (i *BlogServiceImpl) Init() error {
	i.db = conf.C().MySQL.GetDB()
	return nil
}

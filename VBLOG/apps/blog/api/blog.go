package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/YIDADA-0223/VBLOG/apps/blog"
	"gitlab.com/YIDADA-0223/VBLOG/apps/token"
	"gitlab.com/YIDADA-0223/VBLOG/apps/user"
	"gitlab.com/YIDADA-0223/VBLOG/common"
	"gitlab.com/YIDADA-0223/VBLOG/exception"
	"gitlab.com/YIDADA-0223/VBLOG/middleware"
	"gitlab.com/YIDADA-0223/VBLOG/response"
)

func (h *BlogApiHandler) Registry(appRouter gin.IRouter) {
	// 不需要鉴权，公开访问
	appRouter.GET("/", h.QueryBlog)
	appRouter.Use(middleware.Auth)
	appRouter.GET("/:id", h.DescribeBlog)
	// 需要变更需要认证
	// appRouter.POST("/", h.CreateBlog)
	// appRouter.PUT("/:id", h.PutUpdateBlog)
	// appRouter.PATCH("/:id", h.PathUpdateBlog)
	// appRouter.POST("/:id/status", h.UpdateBlogStatus)
	// appRouter.DELETE("/:id", h.DeleteBlog)
	appRouter.POST("/", middleware.RequireRole(user.ROLE_AUTHOR), h.CreateBlog)
	appRouter.PUT("/:id", middleware.RequireRole(user.ROLE_AUTHOR), h.PutUpdateBlog)
	appRouter.PATCH("/:id", middleware.RequireRole(user.ROLE_AUTHOR), h.PathUpdateBlog)
	appRouter.POST("/:id/status", middleware.RequireRole(user.ROLE_AUTHOR), h.UpdateBlogStatus)
	appRouter.DELETE("/:id", middleware.RequireRole(user.ROLE_AUTHOR), h.DeleteBlog)
}
func (h *BlogApiHandler) CreateBlog(ctx *gin.Context) {
	req := blog.NewCreateBlogRequest()
	if err := ctx.Bind(req); err != nil {
		response.Failed(exception.ErrValidateFailed(err.Error()), ctx)
		return
	}
	if v, ok := ctx.Get(token.GIN_TOKEN_KEY_NAME); ok {
		req.CreateBy = v.(*token.Token).UserName
	}
	ins, err := h.svc.CreateBlog(ctx.Request.Context(), req)
	if err != nil {
		response.Failed(err, ctx)
	}
	response.Success(ins, ctx)
}
func (h *BlogApiHandler) QueryBlog(ctx *gin.Context) {
	req := blog.NewQueryBlogRequest()
	req.PageRequest = common.NewPageRequestFromGinCtx(ctx)
	req.KeyWord = ctx.Query("key_word")
	set, err := h.svc.QueryBlog(ctx.Request.Context(), req)
	if err != nil {
		response.Failed(err, ctx)
		return
	}
	response.Success(set, ctx)
}
func (h *BlogApiHandler) DescribeBlog(ctx *gin.Context) {
	req := blog.NewDescribeBlogRequest(ctx.Param("id"))
	ins, err := h.svc.DescribeBlog(ctx.Request.Context(), req)
	if err != nil {
		response.Failed(err, ctx)
	}
	response.Success(ins, ctx)
	return
}
func (h *BlogApiHandler) PutUpdateBlog(ctx *gin.Context) {
	req := blog.NewUpdataBlogRequest(ctx.Param("id"))
	req.UpdateMode = common.UPDATE_MODE_PUT
	if err := ctx.Bind(req); err != nil {
		response.Failed(exception.ErrValidateFailed(err.Error()), ctx)
		return
	}
	set, err := h.svc.UpdateBlog(ctx.Request.Context(), req)
	if err != nil {
		response.Failed(err, ctx)
	}
	response.Success(set, ctx)
}
func (h *BlogApiHandler) PathUpdateBlog(ctx *gin.Context) {
	req := blog.NewUpdataBlogRequest(ctx.Param("id"))
	req.UpdateMode = common.UPDATE_MODE_PATCH
	if err := ctx.Bind(req); err != nil {
		response.Failed(exception.ErrValidateFailed(err.Error()), ctx)
		return
	}
	set, err := h.svc.UpdateBlog(ctx.Request.Context(), req)
	if err != nil {
		response.Failed(err, ctx)
	}
	response.Success(set, ctx)
}
func (h *BlogApiHandler) UpdateBlogStatus(ctx *gin.Context) {
	req := blog.NewUpdateBlogStatusRequest(ctx.Param("id"))
	if err := ctx.Bind(req); err != nil {
		response.Failed(exception.ErrValidateFailed(err.Error()), ctx)
		return
	}
	ins, err := h.svc.UpdateBlogStatus(ctx.Request.Context(), req)
	if err != nil {
		response.Failed(err, ctx)
	}
	response.Success(ins, ctx)
}
func (h *BlogApiHandler) DeleteBlog(ctx *gin.Context) {
	req := blog.NewDeleteBlogRequest(ctx.Param("id"))
	ins, err := h.svc.DeleteBlog(ctx.Request.Context(), req)
	if err != nil {
		response.Failed(err, ctx)
		return
	}
	response.Success(ins, ctx)
}

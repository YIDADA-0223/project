package api

import (
	"gitlab.com/YIDADA-0223/VBLOG/apps/blog"
	"gitlab.com/YIDADA-0223/VBLOG/conf"
	"gitlab.com/YIDADA-0223/VBLOG/ioc"
)

func NewBlogApiHandler() *BlogApiHandler {
	return &BlogApiHandler{
		svc: ioc.Controller.Get(blog.AppName).(blog.Service),
	}
}

// 注册对象
func init() {
	ioc.Api.Registry(blog.AppName, &BlogApiHandler{})
}

// 核心处理API请求
type BlogApiHandler struct {
	svc blog.Service
}

func (h *BlogApiHandler) Init() error {
	h.svc = ioc.Controller.Get(blog.AppName).(blog.Service)
	subRouter := conf.C().Application.GinRootRouter().Group("blogs")
	h.Registry(subRouter)
	return nil
}

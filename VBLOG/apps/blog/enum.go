package blog

type Status int

const (
	// 草稿
	STATUS_DRAFT = iota // 0
	// 已发布
	STATUS_PUBLISH // 1
)

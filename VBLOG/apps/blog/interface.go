package blog

import (
	"context"

	"gitlab.com/YIDADA-0223/VBLOG/common"
	"gitlab.com/YIDADA-0223/VBLOG/exception"
)

const (
	AppName = "blogs"
)

type Service interface {
	// 文章列表查询
	QueryBlog(context.Context, *QueryBlogRequest) (*BlogSet, error)
	// 文章详情
	DescribeBlog(context.Context, *DescribeBlogRequest) (*Blog, error)
	// 文章创建
	CreateBlog(context.Context, *CreateBlogRequest) (*Blog, error)
	// 文章更新
	UpdateBlog(context.Context, *UpdateBlogRequest) (*Blog, error)
	// 文章删除
	DeleteBlog(context.Context, *DeleteBlogRequest) (*Blog, error)
	// 文章发布
	UpdateBlogStatus(context.Context, *UpdateBlogStatusRequest) (*Blog, error)
}

func NewQueryBlogRequest() *QueryBlogRequest {
	return &QueryBlogRequest{
		PageRequest: common.NewPageRequest(),
	}
}

type QueryBlogRequest struct {
	*common.PageRequest
	KeyWord string `json:"keywords"`
}

func NewDescribeBlogRequest(id string) *DescribeBlogRequest {
	return &DescribeBlogRequest{
		BlogId: id,
	}
}

type DescribeBlogRequest struct {
	BlogId string `json:"blog_id"`
}

func (req *UpdateBlogRequest) Validate() error {
	if req.CreateBlogRequest == nil {
		return exception.ErrValidateFailed("CreateBlogRequest required")
	}
	return common.Validate(req)
}
func NewUpdataBlogRequest(id string) *UpdateBlogRequest {
	return &UpdateBlogRequest{
		BlogId: id,
		// 默认是全量
		UpdateMode:        common.UPDATE_MODE_PUT,
		CreateBlogRequest: NewCreateBlogRequest(),
	}
}

type UpdateBlogRequest struct {
	BlogId             string             `json:"blog_id" validate:"required"`
	UpdateMode         common.UPDATE_MODE `json:"update_mode"`
	*CreateBlogRequest `validate:"required"`
}

func NewDeleteBlogRequest(id string) *DeleteBlogRequest {
	return &DeleteBlogRequest{
		BlogId: id,
	}
}

type DeleteBlogRequest struct {
	BlogId string `json:"blog_id"`
}

func NewUpdateBlogStatusRequest(id string) *UpdateBlogStatusRequest {
	return &UpdateBlogStatusRequest{
		BlogId:                   id,
		ChangedBlogStatusRequest: &ChangedBlogStatusRequest{},
	}
}

type UpdateBlogStatusRequest struct {
	BlogId string `json:"blog_id"`
	*ChangedBlogStatusRequest
}

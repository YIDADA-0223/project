package impl

import (
	"context"

	"gitlab.com/YIDADA-0223/VBLOG/apps/user"
	"gitlab.com/YIDADA-0223/VBLOG/common"
	"gitlab.com/YIDADA-0223/VBLOG/conf"
	"gitlab.com/YIDADA-0223/VBLOG/ioc"
	"gorm.io/gorm"
)

func init() {
	ioc.Controller.Registry(user.AppName, &UserServiceImpl{})
}

// func NewUserServiceImpl() *UserServiceImpl {
// 	return &UserServiceImpl{
// 		db: conf.C().MySQL.GetDB(),
// 	}
// }

type UserServiceImpl struct {
	db *gorm.DB
}

func (i *UserServiceImpl) Init() error {
	i.db = conf.C().MySQL.GetDB()
	return nil
}

func (i *UserServiceImpl) CreateUser(ctx context.Context, in *user.CreateUserRequest) (*user.User, error) {
	if err := common.Validate(in); err != nil {
		return nil, err
	}
	if err := in.HashPassword(); err != nil {
		return nil, err
	}
	ins := user.NewUser(in)
	if err := i.db.Save(ins).Error; err != nil {
		return nil, err
	}
	return ins, nil
}

func (i *UserServiceImpl) QueryUser(ctx context.Context, in *user.QueryUserRequest) (*user.UserSet, error) {
	set := user.NewUserSet()
	//根据TableName select
	//WithContext
	query := i.db.Model(&user.User{}).WithContext(ctx)
	if in.Username != "" {
		query = query.Where("username = ?", in.Username)
	}
	if err := query.Count(&set.Total).Error; err != nil {
		return nil, err
	}
	if err := query.Offset(in.Offset()).Limit(in.PageSize).Find(&set.Items).Error; err != nil {
		return nil, err
	}
	return set, nil
}

package user

import (
	"context"

	"gitlab.com/YIDADA-0223/VBLOG/common"
)

const (
	// 业务包名称，用于托管这个业务包的业务对象，Service的具体实现
	AppName = "user"
)

type Service interface {
	CreateUser(context.Context, *CreateUserRequest) (*User, error)
	QueryUser(context.Context, *QueryUserRequest) (*UserSet, error)
}

func NewQueryUserRequest() *QueryUserRequest {
	return &QueryUserRequest{
		PageRequest: common.NewPageRequest(),
	}
}

type QueryUserRequest struct {
	Username string
	*common.PageRequest
}

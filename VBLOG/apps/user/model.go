package user

import (
	"encoding/json"
	"fmt"

	"gitlab.com/YIDADA-0223/VBLOG/common"
	"golang.org/x/crypto/bcrypt"
)

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{
		Role:  ROLE_VISITOR, //默认角色
		Label: map[string]string{},
	}
}

// 用户创建的参数
type CreateUserRequest struct {
	//validate: 校验
	Username string `json:"username" validate:"required" gorm:"column:username"`
	Password string `json:"password" validate:"required" gorm:"column:password"`
	Role     Role   `json:"role" gorm:"column:role"`
	// 用户标签
	Label map[string]string `json:"label" gorm:"column:label;serializer:json"`
}

// 校验器
func (req *CreateUserRequest) Validate() error {
	if req.Username == "" {
		return fmt.Errorf("用户名必填")
	}
	return nil
}

func (req *CreateUserRequest) HashPassword() error {
	cryptoPass, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	req.Password = string(cryptoPass)
	return nil
}

func (req *CreateUserRequest) CheckPassword(password string) error {
	return bcrypt.CompareHashAndPassword([]byte(req.Password), []byte(password))
}

func (c *CreateUserRequest) String() string {
	dj, _ := json.MarshalIndent(c, "", " ")
	return string(dj)
}

type User struct {
	*common.Meta
	//用户参数
	*CreateUserRequest
}

func (req *User) String() string {
	dj, _ := json.MarshalIndent(req, "", " ")
	return string(dj)
}

type UserSet struct {
	//总共有多少个（分页，数据库里面总共）
	Total int64 `json:"total"`
	//对象清单
	Items []*User `json:"items"`
}

// 通用参数
type Meta struct {
	// 用户Id
	Id int `json:"id" gorm:"column:id"`
	// 创建时间, 时间戳 10位, 秒
	CreatedAt int64 `json:"created_at" gorm:"column:created_at"`
	// 更新时间, 时间戳 10位, 秒
	UpdatedAt int64 `json:"updated_at" gorm:"column:updated_at"`
}

func NewUser(req *CreateUserRequest) *User {
	return &User{
		CreateUserRequest: req,
		Meta:              common.NewMeta(),
	}
}
func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}

func (req *User) TableName() string {
	return "users"
}

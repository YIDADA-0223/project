package token

import (
	"encoding/json"
	"time"

	"github.com/rs/xid"
	"gitlab.com/YIDADA-0223/VBLOG/apps/user"
)

func NewToken(u *user.User) *Token {
	return &Token{
		UserId:   u.Id,
		UserName: u.Username,
		//使用随机UUID
		AccessToken: xid.New().String(),
		//过期Token
		AccessTokenExpiredAt:  3600,
		RefreshToken:          xid.New().String(),
		RefreshTokenExpiredAt: 3600 * 4,
		CreatedAt:             time.Now().Unix(),
		Role:                  u.Role,
	}
}

func DefaultToken() *Token {
	return &Token{}
}

type Token struct {
	// 该Token颁发
	UserId int `json:"user_id" gorm:"column:user_id"`
	// 人的名称
	UserName string `json:"username" gorm:"column:username"`
	// 颁发给用户的访问令牌(用户需要携带Token来访问接口)
	AccessToken string `json:"access_token" gorm:"column:access_token"`
	// 过期时间(2h),单位是秒
	AccessTokenExpiredAt int `json:"access_token_expired_at" gorm:"column:access_token_expired_at"`
	// 刷新Token
	RefreshToken string `json:"refresh_token" gorm:"column:refresh_token"`
	// 刷新Token过期时间(7d)
	RefreshTokenExpiredAt int `json:"refresh_token_expired_at" gorm:"column:refresh_token_expired_at"`
	//创建时间
	CreatedAt int64 `json:"created_at" gorm:"column:created_at"`
	//更新时间
	UpdateAt int64 `json:"updated_at" gorm:"column:updated_at"`
	//额外补充信息,gorm忽略处理
	Role user.Role `json:"role" gorm:"-"`
}

// 返回令牌的颁发时间
// 这个函数完成创建颁发时间，并返回一个time.Time类型，以年月日展示
func (t *Token) IssueTime() time.Time {
	return time.Unix(t.CreatedAt, 0)
}

// 返回访问令牌的有效期时长
// 保持类型都是int，使用time.Duration函数，表示时间间隔的类型，指定时间减去当前时间
// Duration相当于Now
func (t *Token) AccessTokenDuration() time.Duration {
	return time.Duration(t.AccessTokenExpiredAt * int(time.Second))
}

// 返回刷新令牌的有效期时长
func (t *Token) RevolkTokenDuration() time.Duration {
	return time.Duration(t.RefreshTokenExpiredAt * int(time.Second))
}

// 颁发Token
func (t *Token) AccessTokenIsExpired() error {
	// 过期时间：颁发时间+过期时长
	//add理解为加时间对象，并返回一个新的时间对象
	//令牌颁发的时间 + 刷新令牌的时间 就等于 过期令牌的时间
	expiredTime := t.IssueTime().Add(t.AccessTokenDuration())
	//当前时间减去过期时间
	// time.Since是用于计算从某个时间点到现在经过的时间的函数，并返回一个time.Duration类型
	if time.Since(expiredTime).Seconds() > 0 {
		return ErrAccessTokenExpired
	}
	return nil
}

// 过期Token
func (t *Token) RefreshTokenIsExpired() error {
	expiredTime := t.IssueTime().Add(t.RevolkTokenDuration())
	// 当前时间减去过期时间
	if time.Since(expiredTime).Seconds() > 0 {
		return ErrRefreshTokenExpired
	}
	return nil
}

func (t *Token) String() string {
	dj, _ := json.MarshalIndent(t, "", " ")
	return string(dj)
}

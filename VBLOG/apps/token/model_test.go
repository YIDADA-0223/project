package token_test

import (
	"testing"
	"time"

	"gitlab.com/YIDADA-0223/VBLOG/apps/token"
	"gitlab.com/YIDADA-0223/VBLOG/apps/user"
)

func TestTokenString(t *testing.T) {
	tk := token.Token{
		UserName: "QUYI",
		UserId:   10,
	}
	t.Log(tk)
}
func TestTokenExired(t *testing.T) {
	now := time.Now().Unix()
	tk := token.Token{
		UserId:               1,
		Role:                 user.ROLE_ADMIN,
		AccessTokenExpiredAt: 1,
		CreatedAt:            now,
	}
	t.Log(tk.AccessTokenIsExpired())
}

package token

import (
	"net/http"

	"gitlab.com/YIDADA-0223/VBLOG/exception"
)

var (
	ErrAuthFailed          = exception.NewApiException(50001, "用户名或者密码不正确").WithHttpCode(http.StatusUnauthorized)
	ErrAccessTokenExpired  = exception.NewApiException(50002, "AccessToken过期")
	ErrRefreshTokenExpired = exception.NewApiException(50003, "RefreshToken过期")
	ErrPermissionDenied    = exception.NewApiException(50004, "该角色无法访问当前接口").WithHttpCode(http.StatusForbidden)
)

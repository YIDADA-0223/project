package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/YIDADA-0223/VBLOG/apps/token"
	"gitlab.com/YIDADA-0223/VBLOG/conf"
	"gitlab.com/YIDADA-0223/VBLOG/ioc"
	"gitlab.com/YIDADA-0223/VBLOG/response"
)

// 注册对象
func init() {
	ioc.Api.Registry(token.AppName, &TokenApiHandler{})
}
func (h *TokenApiHandler) Init() error {
	h.token = ioc.Controller.Get(token.AppName).(token.Service)
	subRouter := conf.C().Application.GinRootRouter().Group("tokens")
	h.Registry(subRouter)
	return nil
}
func NewTokenApiHandler() *TokenApiHandler {
	return &TokenApiHandler{
		token: ioc.Controller.Get(token.AppName).(token.Service),
	}
}

// 核心处理API请求
type TokenApiHandler struct {
	token token.Service
}

// 路由注册
func (h *TokenApiHandler) Registry(appRouter gin.IRouter) {
	appRouter.POST("/", h.Login)
	appRouter.DELETE("/", h.Logout)
}

func (h *TokenApiHandler) Login(c *gin.Context) {
	// 1.获取HTTP请求
	req := token.NewIssueTokenRequest("", "")
	if err := c.BindJSON(req); err != nil {
		response.Failed(err, c)
		return
	}
	// 2.业务处理
	tk, err := h.token.IssueToken(c.Request.Context(), req)
	if err != nil {
		response.Failed(err, c)
		return
	}
	c.SetCookie(
		token.COOKIE_TOKEY_KEY,
		tk.AccessToken,
		tk.RefreshTokenExpiredAt,
		"/", conf.C().Application.Domain,
		false, true,
	)

	response.Success(tk, c)
	c.JSON(http.StatusOK, tk)
}

func (h *TokenApiHandler) Logout(c *gin.Context) {
	ak, err := c.Cookie(token.COOKIE_TOKEY_KEY)
	if err != nil {
		response.Failed(err, c)
		return
	}
	rt := c.GetHeader(token.REFRESH_HEADER_KEY)
	req := token.NewRevolkTokenRequest(ak, rt)
	tk, err := h.token.RevolkToken(c.Request.Context(), req)
	if err != nil {
		response.Failed(err, c)
		return
	}
	response.Success(tk, c)
}

package impl_test

import (
	"context"
	"testing"

	"gitlab.com/YIDADA-0223/VBLOG/apps/token"
	"gitlab.com/YIDADA-0223/VBLOG/ioc"
	"gitlab.com/YIDADA-0223/VBLOG/test"
)

var (
	serviceImpl token.Service
	ctx         = context.Background()
)

// 招对象
func init() {
	test.DevelopmentSetup()
	serviceImpl = ioc.Controller.Get(token.AppName).(token.Service)
}

// 颁发Token测试
func TestIssueToken(t *testing.T) {
	req := token.NewIssueTokenRequest("admin", "123456")
	tk, err := serviceImpl.IssueToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk.String())
}

func TestRevolkToken(t *testing.T) {
	req := token.NewRevolkTokenRequest("cpnpftobmte6v72tad0g", "cpnpftobmte6v72tad10")
	tk, err := serviceImpl.RevolkToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestValidateToken(t *testing.T) {
	req := token.NewValidateTokenRequest("cpnpftobmte6v72tad0g")
	tk, err := serviceImpl.ValidateToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

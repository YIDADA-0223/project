package impl

import (
	"context"
	"fmt"

	"gitlab.com/YIDADA-0223/VBLOG/apps/token"
	"gitlab.com/YIDADA-0223/VBLOG/apps/user"
	"gitlab.com/YIDADA-0223/VBLOG/conf"
	"gitlab.com/YIDADA-0223/VBLOG/exception"
	"gitlab.com/YIDADA-0223/VBLOG/ioc"
	"gorm.io/gorm"
)

//	func NewTokenServiceImpl(userServiceImpl user.Service) *TokenServiceImpl {
//		return &TokenServiceImpl{
//			db:   conf.C().MySQL.GetDB(),
//			user: userServiceImpl,
//		}
//	}
func init() {
	ioc.Controller.Registry(token.AppName, &TokenServiceImpl{})
}

type TokenServiceImpl struct {
	db   *gorm.DB
	user user.Service
}

func (i *TokenServiceImpl) Init() error {
	i.db = conf.C().MySQL.GetDB()
	i.user = ioc.Controller.Get(user.AppName).(user.Service)
	return nil
}

func (i *TokenServiceImpl) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (*token.Token, error) {
	//1.查询用户
	queryUser := user.NewQueryUserRequest()
	queryUser.Username = in.Username
	us, err := i.user.QueryUser(ctx, queryUser)
	if err != nil {
		return nil, err
	}
	//如果查询为0，就是没有查到那么久报错
	if len(us.Items) == 0 {
		return nil, fmt.Errorf("查询出错")
	}
	//2.比对用户密码
	u := us.Items[0]
	if err := u.CheckPassword(in.Password); err != nil {
		return nil, token.ErrAuthFailed
	}
	//3.上面密码比对成功，就颁发令牌，随机生成一个多少位字符串
	tk := token.NewToken(u)
	if err := i.db.WithContext(ctx).Create(tk).Error; err != nil {
		return nil, exception.ErrServerInternal("保存报错，%s", err)
	}
	return tk, nil
}
func (i *TokenServiceImpl) RevolkToken(ctx context.Context, in *token.RevolkTokenRequest) (*token.Token, error) {
	// 先查询Token有没有
	tk := token.DefaultToken()
	err := i.db.WithContext(ctx).Where("access_token = ?", in.AccessToken).First(tk).Error
	if err == gorm.ErrRecordNotFound {
		return nil, exception.ErrNotFound("Token未找到")
	}
	if tk.RefreshToken != in.RefreshToken {
		return nil, fmt.Errorf("RefreshToken不正确")
	}
	// 删除
	err = i.db.WithContext(ctx).Where("access_token = ?", in.AccessToken).Delete(token.Token{}).Error
	if err != nil {
		return nil, err
	}
	return tk, nil
}
func (i *TokenServiceImpl) ValidateToken(ctx context.Context, in *token.ValidateTokenRequest) (*token.Token, error) {
	tk := token.DefaultToken()
	err := i.db.WithContext(ctx).Where("access_token = ?", in.AccessToken).First(tk).Error
	if err == gorm.ErrRecordNotFound {
		return nil, exception.ErrNotFound("Token未找到")
	}
	if err != nil {
		return nil, exception.ErrServerInternal("查询报错，%v", err)
	}
	//判断Token是否过期
	if err := tk.RefreshTokenIsExpired(); err != nil {
		return nil, err
	}
	//判断AccessToken是否过期
	if err := tk.AccessTokenIsExpired(); err != nil {
		return nil, err
	}
	// 4. 补充用户角色信息
	queryUserReq := user.NewQueryUserRequest()
	queryUserReq.Username = tk.UserName
	us, err := i.user.QueryUser(ctx, queryUserReq)
	if err != nil {
		return nil, err
	}
	if len(us.Items) == 0 {
		return nil, fmt.Errorf("token user not found")
	}
	tk.Role = us.Items[0].Role
	return tk, nil

}

package token

import (
	"context"
)

const (
	AppName = "token"
)

type Service interface {
	// 颁发令牌
	IssueToken(context.Context, *IssueTokenRequest) (*Token, error)
	// 撤销令牌
	RevolkToken(context.Context, *RevolkTokenRequest) (*Token, error)
	// 校验令牌
	ValidateToken(context.Context, *ValidateTokenRequest) (*Token, error)
}

// 颁发令牌构造方法
// 提供了username,password形参，用于调用者提供初始化值
func NewIssueTokenRequest(username, password string) *IssueTokenRequest {
	return &IssueTokenRequest{
		Username: username,
		Password: password,
		IsMember: false,
	}
}

// 颁发令牌
type IssueTokenRequest struct {
	Username string
	Password string
	IsMember bool
}

func NewRevolkTokenRequest(at, rt string) *RevolkTokenRequest {
	return &RevolkTokenRequest{
		AccessToken:  at,
		RefreshToken: rt,
	}
}

// 撤销令牌
type RevolkTokenRequest struct {
	AccessToken  string
	RefreshToken string
}

func NewValidateTokenRequest(at string) *ValidateTokenRequest {
	return &ValidateTokenRequest{
		AccessToken: at,
	}
}

// 校验令牌
type ValidateTokenRequest struct {
	AccessToken string
}

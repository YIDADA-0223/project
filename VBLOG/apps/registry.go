package apps

import (
	// 业务对象注册
	_ "gitlab.com/YIDADA-0223/VBLOG/apps/blog/api"
	_ "gitlab.com/YIDADA-0223/VBLOG/apps/blog/impl"
	_ "gitlab.com/YIDADA-0223/VBLOG/apps/token/api"
	_ "gitlab.com/YIDADA-0223/VBLOG/apps/token/impl"
	_ "gitlab.com/YIDADA-0223/VBLOG/apps/user/impl"
)
